from django.contrib import admin

from .models import Territory, Currency

admin.site.register(Territory)
admin.site.register(Currency)
