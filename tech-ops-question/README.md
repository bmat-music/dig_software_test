# Digital - Technical Operations Engineer

A lot of our work consist on helping the operations team on their daily tasks,
sometimes just converting a repetitive process into a single command.

In this task, we would need to imagine that we signed a new deal with several
territories, and we want to add them in our database. A solution for that
would be to add the territories one by one using the admin page. But as the
delivery is quite urgent, we would like to do it in a single command.

To achieve this, the operations team has already gathered all the territories
and currencies that we need to ingest, so we should help them only with this
ingestion.

### Deliverables

+ The code to import all the contents from data/territories_to_add.csv to the
  database.
+ Explanation on how should we run the code (you can edit this README.md or
  add another file)

### Database tables

+ Territory: Models territory
+ Currency: Models currency

### Requirements

+ Django 3.2.5
+ Python 3.9

### Useful resources

- Django tutorial: https://docs.djangoproject.com/en/3.2/intro/tutorial01/


### Notes

In order to manage python dependencies, it will be necessary to use any tool (e.g.: pipenv) that interprets the Pipfile placed in the root folder.

For example, using pipenv, it's enough to do:
```
pipenv sync --dev
```
