from . import views

from django.conf.urls import url

urlpatterns = [
    url(r'^api/dsrs/count$', views.count, name='count'),
    url(r'^api/dsrs/(?P<pk>[0-9]+)/$', views.dsr, name='count'),
]
