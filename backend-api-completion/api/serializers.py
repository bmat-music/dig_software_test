from rest_framework import serializers

from api import models


class DSRSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.DSR
        fields = '__all__'
