from api import models, serializers

from rest_framework.decorators import api_view
from rest_framework.response import Response


@api_view(['GET'])
def count(request):
    return Response(headers={'X-Total-Count': models.DSR.objects.count()})


@api_view(['GET'])
def dsr(request, pk):
    try:
        dsr = models.DSR.objects.get(pk=pk)
        data = serializers.DSRSerializer(dsr).data
    except models.DSR.DoesNotExist:
        data = None
    return Response(
        {'data': data, },
        headers={'X-Total-Count': 1 if data else 0}
    )
