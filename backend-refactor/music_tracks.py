import csv



class MusicTracks:
  @staticmethod
  def where(options = {}):
    with open("./tracks.csv", "rt") as csvfile:
      data = csv.reader(csvfile, delimiter=',', quotechar='"')
      csv_data = []
      for row in data:
        csv_data.append(row)


    if 'track_id' in options:
      result = []
      for row in csv_data:
        if row[0] == options['track_id']:
          result.append(row)
      csv_data = result

    if 'album_id' in options:
      result = []
      for row in csv_data:
        if row[1] == options['album_id']:
          result.append(row)
      csv_data = result


    if 'album_title' in options:
      result = []
      for row in csv_data:
        if row[2] == options['album_title']:
          result.append(row)
      csv_data = result

    if 'album_url' in options:
      result = []
      for row in csv_data:
        if row[3] == options['album_url']:
          result.append(row)
      csv_data = result

    if 'artist_id' in options:
      result = []
      for row in csv_data:
        if row[4] == options['artist_id']:
          result.append(row)
      csv_data = result

    if 'artist_name' in options:
      result = []
      for row in csv_data:
        if row[5] == options['artist_name']:
          result.append(row)
      csv_data = result
    if 'artist_url' in options:
      result = []
      for row in csv_data:
        if row[6] == options['artist_url']:
          result.append(row)
      csv_data = result

    if 'track_date_created' in options:
      result = []
      for row in csv_data:
        if row[7] == options['track_date_created']:
          result.append(row)
      csv_data = result

    if 'track_duration' in options:
      result = []
      for row in csv_data:
        if row[8] == options['track_duration']:
          result.append(row)
      csv_data = result

    if 'track_listens' in options:
      result = []
      for row in csv_data:
        if row[9] == options['track_listens']:
          result.append(row)
      csv_data = result

    if 'track_title' in options:
      result = []
      for row in csv_data:
        if row[10] == options['track_title']:
          result.append(row)
      csv_data = result


    if 'track_url' in options:
      result = []
      for row in csv_data:
        if row[11] == options['track_url']:
          result.append(row)
      csv_data = result


    output = []
    for row in csv_data:
      mapped={}
      mapped['track_id'] = row[0]
      mapped['album_id'] = row[1]
      mapped['album_title'] = row[2]
      mapped['album_url'] = row[3]
      mapped['artist_id'] = row[4]
      mapped['artist_name'] = row[5]
      mapped['artist_url'] = row[6]
      mapped['track_date_created'] = row[7]
      mapped['track_duration'] = row[8]
      mapped['track_listens'] = row[9]
      mapped['track_title'] = row[10]
      mapped['track_url'] = row[11]
      output.append(mapped)

    return output

  @staticmethod
  def find_by_id(options):
    with open("./tracks.csv", "rt") as csvfile:
      data = csv.reader(csvfile, delimiter=',', quotechar='"')
      csv_data = []
      for row in data:
        csv_data.append(row)

    if 'track_id' in options:
      for row in csv_data:
        if row[0] == options['track_id']:
          mapped = {}
          mapped['track_id'] = row[0]
          mapped['album_id'] = row[1]
          mapped['album_title'] = row[2]
          mapped['album_url'] = row[3]
          mapped['artist_id'] = row[4]
          mapped['artist_name'] = row[5]
          mapped['artist_url'] = row[6]
          mapped['track_date_created'] = row[7]
          mapped['track_duration'] = row[8]
          mapped['track_listens'] = row[9]
          mapped['track_title'] = row[10]
          mapped['track_url'] = row[11]
          return mapped


    if 'album_id' in options:
      for row in csv_data:
        if row[1] == options['album_id']:
          mapped = {}
          mapped['track_id'] = row[0]
          mapped['album_id'] = row[1]
          mapped['album_title'] = row[2]
          mapped['album_url'] = row[3]
          mapped['artist_id'] = row[4]
          mapped['artist_name'] = row[5]
          mapped['artist_url'] = row[6]
          mapped['track_date_created'] = row[7]
          mapped['track_duration'] = row[8]
          mapped['track_listens'] = row[9]
          mapped['track_title'] = row[10]
          mapped['track_url'] = row[11]
          return mapped

    if 'artist_id' in options:
      for row in csv_data:
        if row[4] == options['artist_id']:
          mapped = {}
          mapped['track_id'] = row[0]
          mapped['album_id'] = row[1]
          mapped['album_title'] = row[2]
          mapped['album_url'] = row[3]
          mapped['artist_id'] = row[4]
          mapped['artist_name'] = row[5]
          mapped['artist_url'] = row[6]
          mapped['track_date_created'] = row[7]
          mapped['track_duration'] = row[8]
          mapped['track_listens'] = row[9]
          mapped['track_title'] = row[10]
          mapped['track_url'] = row[11]
          return mapped


    raise RecordNotFound

class RecordNotFound(Exception):
  pass
