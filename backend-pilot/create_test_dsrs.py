import argparse

import dsr


def main(args):
    dsr_crafter = dsr.Crafter(args.output_dir)
    dsr_crafter.craft(
        args.num_complete_overlapping,
        args.num_overlapping_except_isrc,
        args.num_overlapping_without_revenue,
        args.num_overlapping_without_usages,
        args.num_overlapping_without_revenue_and_usages,
        args.num_incomplete_resources,
        args.num_complete_resources,
        seed=args.seed,
        shuffle=args.shuffle
    )


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Creates DSRs with the following characteristics'
    )
    parser.add_argument(
        '--output_dir',
        help='Output directory',
        default='./test_files/',
    )
    parser.add_argument(
        '--num_complete_overlapping',
        help='Number of Resources overlapping among all DSRs',
        type=int,
        default=25,
    )
    parser.add_argument(
        '--num_overlapping_except_isrc',
        help='Resources overlapping among all DSRs, but with different ISRCs',
        type=int,
        default=25,
    )
    parser.add_argument(
        '--num_overlapping_without_revenue',
        help='Resources overlapping among all DSRs, but the revenue is'
             'missing in some DSRs',
        type=int,
        default=25,
    )
    parser.add_argument(
        '--num_overlapping_without_usages',
        help='Resources overlapping among all DSRs, but the usages are '
             'missing in some DSRs',
        type=int,
        default=25,
    )
    parser.add_argument(
        '--num_overlapping_without_revenue_and_usages',
        help='Resources overlapping among all DSRs, but the revenue and usages'
             ' are missing in some DSRs',
        type=int,
        default=25,
    )
    parser.add_argument(
        '--num_incomplete_resources',
        help='Number of (probably unrelated) resources with missing sound '
             'recording metadata (title, artists or isrcs)',
        type=int,
        default=25,
    )
    parser.add_argument(
        '--num_complete_resources',
        help='Number of (probably unrelated) resources with complete metadata',
        type=int,
        default=25,
    )
    parser.add_argument(
        '--seed',
        help='Random seed to reproduce the files',
        type=int,
        default=12345,
    )
    parser.add_argument(
        '--shuffle',
        help='Shuffle resources in each DSR. Set to False for debugging '
             'purposes.',
        action='store_true',
        default=True,
    )

    args = parser.parse_args()
    main(args)
