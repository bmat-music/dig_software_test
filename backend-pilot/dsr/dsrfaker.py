import typing as tp

from faker import Faker
from faker.providers import BaseProvider

from dsr import constants as c


LOCALE = 'en-US'

Resource = tp.Dict[str, tp.Any]


class Provider(BaseProvider):

    def dsp_id(self) -> str:
        return self.generator.pystr(30, 30)

    def title(self) -> str:
        n_words = self.random_int(2, 4)
        return ' '.join(self.generator.words(n_words))

    def artists(self, n: tp.Optional[int] = None) -> tp.List[str]:
        n = self.random_int(1, 5) if n is None else n
        return [self.generator.name() for _ in range(n)]

    def isrc(self, separator='') -> str:
        return separator.join([
            self.generator.country_code('alpha-2'),                  # country
            self.generator.pystr(min_chars=3, max_chars=3).upper(),  # licensor
            '{}'.format(self.random_int(0, 99)).zfill(2),     # year
            '{}'.format(self.random_int(0, 99999)).zfill(5)   # recording
        ])

    def isrc_list(self, n: int = None) -> tp.List[str]:
        n_isrcs = self.random_int(1, 3) if n is None else n
        return [self.isrc() for _ in range(n_isrcs)]

    def usages(self) -> int:
        return self.random_int(min=0, max=1e6)

    def revenue(self) -> float:
        return self.generator.pyfloat(positive=True)

    def resource(self) -> Resource:
        return {
            c.DSP_ID: self.dsp_id(),
            c.TITLE: self.title(),
            c.ARTISTS: '|'.join(self.artists()),
            c.ISRC: self.isrc(),
            c.USAGES: self.usages(),
            c.REVENUE: self.revenue(),
        }

    def resources(self, number_of_resources) -> tp.Iterable[Resource]:
        return iter(self.resource() for _ in range(number_of_resources))


fake = Faker(local=LOCALE)
fake.add_provider(Provider)
