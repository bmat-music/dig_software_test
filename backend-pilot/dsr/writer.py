import typing as tp
import gzip
import csv
import pathlib

from dsr import constants as c


class Writer:

    header = [
        c.DSP_ID,
        c.TITLE,
        c.ARTISTS,
        c.ISRC,
        c.USAGES,
        c.REVENUE,
    ]

    """Generate a fake DSR with the fields specified in header"""
    def __init__(
        self,
        dsp_name: str,
        society_name: str,
        commercial_offer: str,
        period: str,
        territory_code: str,
        currency_code: str,
        output_dir: str,
    ):
        self.dsp_name = dsp_name
        self.society_name = society_name
        self.commercial_offer = commercial_offer
        self.period = period
        self.territory_code = territory_code
        self.currency_code = currency_code
        self._output_dir = pathlib.Path(output_dir)
        self._fd = None
        self._writer = None

    def writeheader(self):
        self._writer.writeheader()

    def writerow(self, row: tp.Dict[str, tp.Any]):
        self._writer.writerow(row)

    def open(self):
        filename = f'{self.dsp_name}_{self.commercial_offer}_' \
                   f'{self.society_name}_{self.territory_code}_' \
                   f'{self.currency_code}_{self.period}.tsv.gz'
        self._fd = gzip.open(f'{self._output_dir / filename}', 'wt')
        self._writer = csv.DictWriter(
            self._fd,
            fieldnames=self.header,
            delimiter='\t',
        )
        return self._fd

    def close(self):
        self._writer = None
        self._fd.close()
        self._fd = None

    def __enter__(self):
        self.open()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()
