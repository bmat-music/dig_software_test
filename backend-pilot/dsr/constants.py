# DSR
DSP_ID = 'dsp_id'
TITLE = 'title'
ARTISTS = 'artists'
ISRC = 'isrc'
USAGES = 'usages'
REVENUE = 'revenue'

# TERRITORY CODES
CH = 'CH'
ES = 'ES'
GB = 'GB'
NO = 'NO'
TERRITORIES = [CH, ES, GB, NO]
