import typing as tp
import random

from collections import defaultdict
from copy import deepcopy

from dsr import constants as c
from dsr.dsrfaker import fake, Resource
from dsr.writer import Writer


class Crafter:

    @classmethod
    def _shuffle(cls, resources: tp.List[Resource]) -> tp.List[Resource]:
        return random.sample(resources, k=len(resources))

    def __init__(self, output_dir):
        self._dsr_writers: tp.Dict[str, Writer] = {}
        self._resources: tp.Dict[str, tp.List[Resource]] = defaultdict(
            lambda: []
        )
        self._init_dsrs(output_dir)

    def craft(
        self,
        num_complete_overlapping: int,
        num_overlapping_except_isrc: int,
        num_overlapping_without_revenue: int,
        num_overlapping_without_usages: int,
        num_overlapping_without_revenue_and_usages: int,
        num_incomplete_resources: int,
        num_complete_resources: int,
        seed: int = 12345,
        shuffle: bool = True,
    ):
        """Creates DSRs with the following characteristics:

            - Resources overlapping among all DSRs.
            - Resources overlapping among all DSRs, but with different ISRCs
            - Resources overlapping among all DSRs, but the revenue is
              missing in some DSRs.
            - Resources overlapping among all DSRs, but the usages are
              missing in some DSRs.
            - Resources overlapping among all DSRs, but bot revenue and usages
              are missing in some DSRs.
            - Resources (probably unrelated) with some missing metadata
              (artists, titles or isrcs).
            - Resources (probably unrelated) with no missing metadata.
        """
        random.seed(seed)

        # Generate N resources that will overlap in all DSRs
        for resource in fake.resources(num_complete_overlapping):
            for territory_code in c.TERRITORIES:
                self._add(territory_code, deepcopy(resource))

        # Resources overlapping among all DSRs, but with different ISRCs
        for resource in fake.resources(num_overlapping_except_isrc):
            for territory_code in c.TERRITORIES:
                resource = deepcopy(resource)
                resource[c.ISRC] = fake.isrc()
                self._add(territory_code, resource)

        # Resources overlapping among all DSRs, but the revenue is missing in
        # some DSRs
        for resource in fake.resources(num_overlapping_without_revenue):
            for territory_code in c.TERRITORIES:
                resource = deepcopy(resource)
                if random.randint(0, 1):
                    resource[c.REVENUE] = ''
                self._add(territory_code, resource)

        # Resources overlapping among all DSRs, but the revenue is missing in
        # some DSRs
        for resource in fake.resources(num_overlapping_without_usages):
            for territory_code in c.TERRITORIES:
                resource = deepcopy(resource)
                if random.randint(0, 1):
                    resource[c.USAGES] = ''
                self._add(territory_code, resource)

        # Resources overlapping among all DSRs, but the revenue is missing in
        # some DSRs
        resources = fake.resources(num_overlapping_without_revenue_and_usages)
        for resource in resources:
            for territory_code in c.TERRITORIES:
                resource = deepcopy(resource)
                if random.randint(0, 1):
                    resource[c.REVENUE] = ''
                    resource[c.USAGES] = ''
                self._add(territory_code, resource)

        # Number of (probably unrelated) resources with missing sound
        # recording metadata (title, artists or isrcs)
        for _ in range(num_incomplete_resources):
            for territory_code in c.TERRITORIES:
                resource = fake.resource()
                field = random.choice(
                    [c.TITLE, c.ARTISTS, c.ISRC],
                )
                resource[field] = ''
                self._add(territory_code, resource)

        # Number of (probably unrelated) resources with complete metadata
        for _ in range(num_complete_resources):
            for territory_code in c.TERRITORIES:
                resource = fake.resource()
                self._add(territory_code, resource)

        # self._validate_dsr()
        self._write_dsrs(shuffle)

    def _init_dsrs(self, output_dir):
        self._dsr_writers = {
            c.CH: Writer(
                dsp_name='Spotify',
                society_name='SACEM',
                commercial_offer='SpotifyFree',
                period='20200201-20200228',  # Monthly
                territory_code=c.CH,
                currency_code='CHF',
                output_dir=output_dir,
            ),
            c.GB: Writer(
                dsp_name='Spotify',
                society_name='SGAE',
                commercial_offer='SpotifyStudent',
                period='20200101-20200430',  # Trimesterly
                territory_code=c.GB,
                currency_code='GBP',
                output_dir=output_dir,
            ),
            c.ES: Writer(
                dsp_name='Spotify',
                society_name='SGAE',
                commercial_offer='SpotifyFamilyPlan',
                period='20200101-20200331',  # Quarterly
                territory_code=c.ES,
                currency_code='EUR',
                output_dir=output_dir,
            ),
            c.NO: Writer(
                dsp_name='Spotify',
                society_name='SGAE',
                commercial_offer='SpotifyDuo',
                period='20200101-20200531',  # Semesterly
                territory_code=c.NO,
                currency_code='NOK',
                output_dir=output_dir,
            ),
        }

    def _add(self, territory_code: str, resource: Resource):
        self._resources[territory_code].append(resource)

    def _validate_dsr(self):
        raise NotImplementedError

    def _write_dsrs(self, shuffle):
        for territory_code, dsrwriter in self._dsr_writers.items():
            with dsrwriter as writer:
                writer.writeheader()
                resources = self._resources[territory_code]
                if shuffle:
                    resources = self._shuffle(resources)
                for resource in resources:
                    writer.writerow(resource)
