# Generate test dsrs
Test files have been generated with the following command:

```
python create_test_dsrs.py --num_complete_overlapping 38 --num_overlapping_except_isrc 13 --num_overlapping_without_revenue 17 --num_overlapping_without_usages 9  --num_overlapping_without_revenue_and_usages 6 --num_incomplete_resources 127 --num_complete_resources 790 --seed 12345
```
